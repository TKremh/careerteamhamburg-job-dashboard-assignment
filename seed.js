'use strict'

const mysql = require('mysql')
const config = require('config')
const environment = process.argv[2]
const seedName = process.argv[3]

let seeds = config.get('careerteam.environments.' + environment + '.seeds')

const connection = mysql.createConnection(config.get('careerteam.environments.' + environment + '.mysql'))

// only use one seed
if (seedName) {
  for (let i = 0; i < seeds.length; i++) {
    if (seeds[i].table_name === seedName) {
      seeds = [seeds[i]]
    }
  }
}

for (let i = 0; i < seeds.length; i++) {
  const seed = require(seeds[i].seed)
  for (let j = 0; j < seed.length; j++) {
    const seedItem = seed[j]

    if (seeds[i].table_name) {
      if (typeof seedItem.claims === 'object') {
        seedItem.claims = JSON.stringify(seedItem.claims)
      }
      if (typeof seedItem.badges === 'object') {
        seedItem.badges = JSON.stringify(seedItem.badges)
      }
    }

    let colNames = []
    let placeholders = []
    let values = []
    for (const key in seedItem) {
      //begin my changes
      const value = convertDateStringToMysqlDateTime(key,seedItem[key]);
      //console.log(value)

      //end my changes
      colNames.push(key)
      placeholders.push('?')
      values.push(value)
      //values.push(seedItem[key])
    }

    const query = `
      INSERT INTO ${ seeds[i].table_name }(
        ${ colNames.join(', ') }
      )
      VALUES(
        ${ placeholders.join(', ') }
      )
    `

     connection.query(
       query,
       values,
       (err, results, fields) => {
         if (err) {
           return console.log(err)
         }
         console.log('successfully inserted item')
       }
     )
  }
}

connection.end()

//added to get seeding to work
function convertDateStringToMysqlDateTime(collumName, value){
  //console.log('collumName:' + collumName + ' value: '+ value)
  if(collumName==='created_at' || collumName==='updated_at'
    || collumName==='briefing_call' || collumName==='closed_at'){
    const d = new Date(value);
    return d.toISOString().slice(0, 19).replace('T', ' ');
  }else{
    return value;
  }
}
