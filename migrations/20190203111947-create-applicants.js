'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('applicants', {
      id: {
        allowNull: false,
     //   autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      first_name:{
        type: Sequelize.STRING
      },
      last_name:{
        type: Sequelize.STRING
      },
      email:{
        type: Sequelize.STRING
      },
      phone:{
        type: Sequelize.STRING
      },
      current_company:{
        type: Sequelize.STRING
      },
      current_position:{
        type: Sequelize.STRING
      },
      created_at:{
        type: Sequelize.DATE
      },
      updated_at:{
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('applicants');
  }
};
