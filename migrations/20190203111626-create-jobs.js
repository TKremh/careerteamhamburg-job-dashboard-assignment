'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('jobs', {
      id: {
        allowNull: false,
  //      autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      position_name:{
        type: Sequelize.STRING
      },
      recruiter_name:{
        type: Sequelize.STRING
      },
      company_name:{ type: Sequelize.STRING
      },
      briefing_call:{
        type: Sequelize.DATE
      },
      contacted:{
        type: Sequelize.INTEGER
      },
      positive_response:{
        type: Sequelize.INTEGER
      },
      interview:{
        type:Sequelize.INTEGER
      },
      forward:{
        type: Sequelize.INTEGER
      },
      closed_at:{
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('jobs');
  }
};
