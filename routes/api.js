const express = require('express');
const router = express.Router();

const jobs = require('../models').jobs;
const applicants = require('../models').applicants;
//const applicant_jobs = require('../models').applicant_jobs;

//Routes APIs
//Routes API JOBS

// get all jobs
router.get('/jobs', (req,res) => {
  jobs.findAll({
    raw:false,
    include:[{
      model:applicants,
      as:'listApplicants',
      require:false,
      raw:false
      }]
  }).then(jobs => res.send(jobs));
});

// get job by id
router.get('/jobs/:id', (req,res) => {
  jobs.findById(req.params.id,{
    include:[{
      model:applicants,
      as:'listApplicants',
      require:false,
      raw:false
      }]
  })
  .then(jobs => res.send(jobs))
  .catch(err => res.status(404).send('Error'));
});

// add new job to db
router.post('/jobs', (req,res) => {
  //2DO validation and database insert
  res.status(404).send('not yet implemented!');
});

//delete job by id from bd
router.delete('/jobs/:id', (req, res) => {
  //2DO validation and database insert
  res.status(404).send('not yet implemented!');
});

//Routes API APPLICANTS
router.get('/applicants', (req,res) => {
  res.status(404).send('not yet implemented!');
});

router.get('/applicants/:id', (req, res) => {
  res.status(404).send('not yet implemented!');
});

router.post('/applicants', (req,res) => {
  //2DO validation and database insert
  res.status(404).send('not yet implemented!');
});

router.delete('/applicants/:id', (req, res) => {
  //2DO validation and database insert
  res.status(404).send('not yet implemented!');
});

//Routes API Join TABLE
router.get('/applicant_jobs', (req,res) => {
  //2DO validation and database lookup
  res.status(404).send('not yet implemented!');
});

router.get('/applicants/:id', (req,res) => {
  //2DO validation and database lookup
  res.status(404).send('not yet implemented!');
});

router.post('/applicant_jobs', (req,res) => {
  //2DO validation and database insert
  res.status(404).send('not yet implemented!');
});

router.delete('/applicant_jobs/:id', (req, res) => {
  //2DO validation and database removal
  res.status(404).send('not yet implemented!');
});

module.exports = router;
