'use strict'

const mysql = require('mysql');
const migration = require('mysql-migrations');
const config = require('config');

module.exports = (environment) => {
  const connection = mysql.createPool(config.get('careerteam.environments.' + environment + '.mysql'))
  migration.init(connection, __dirname + '/migrations')
}
