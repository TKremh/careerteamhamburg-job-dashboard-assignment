//console.log('hello, world!')

const express = require('express');
const bodyParser = require('body-parser');
const exphbs  = require('express-handlebars');
const path = require('path');

const app = express()
const port = process.env.port || 9000
const apis = require('../routes/api');

const hbs = exphbs.create({
  defaultLayout: 'main',
  layoutsDir: path.join('views/layouts'),
  partialsDir: 'views/partials'

});

// add bodyParser
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

// add static files
app.use('/public', express.static(__dirname + '/public'));

// setup handlebars-express
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

// Routes
app.get('/', (req, res) => {
  res.render('index', {
    title: 'index',
    hostname: req.hostname,
    port : port,
  //  jobs : varJobs
    })
  });

// Routes APIs
app.use('/api',apis);

app.listen(port, () => console.log(`app listening on port ${port}!`));

// for debugging the database only
//-----------------------------------------
//const db = require('./database');
//  db.authenticate()
//     .then(function () {
//         console.log("CONNECTED! ");
//     })
//     .catch(function (err) {
//         console.log(err);
//     })
//     .done();

// jobs.findById(1,{
//   include:[{
//     model:applicants,
//     as:'listApplicants',
//     require:false,
//     raw:false
//   }]
// }).then(jobs => {
//      console.log(jobs.get() );
//      jobs.listApplicants.forEach(
//         applicants => console.log(applicants.get())
//      )
//   });

// db.query('select * from jobs',{
//   model: jobs,
//   mapToModel:true
// }).then(jobs => console.log(jobs));
//-----------------------------------------
