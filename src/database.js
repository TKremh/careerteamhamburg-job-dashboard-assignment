const Sequelize = require('sequelize');

module.exports = new Sequelize('job_dashboard', 'root', 'password', {
    host: 'job_dashboard_fs_mysql',
    port: 3306,
    dialect: 'mysql',
    operatorsAliases: false
  });

