// returns html needed for refreshing the table
function getTemplateString(){
  const templateInfo = '<tr><td>{{position_name}}</td><td>{{recruiter_name}}</td><td>{{company_name}}</td> <td>{{briefing_call}}</td>'+
  '<td><table class="table"><tr><th>positive_response/contacted</th><th>interview/positive_response</th><th>forward/interview</th></tr>'+
  '<tr><td>{{positive_response}}</td><td>{{interview}}</td><td>{{forward}}</td></tr><tr><td>{{contacted}}</td><td>{{positive_response}}</td>'+
  '<td>{{interview}}</td></tr></table></td><td><table class= "table"><tr><th>Applicant Name</th><th>Application status</th></tr>'+
  '{{#handleListApplicants listApplicants }}{{/handleListApplicants}} </table></td></tr>'
  return templateInfo;
}

// returns html needed for refreshing the table
function generateNewTable(){
  const htmlTable = '<table id ="contentTable" class= "table table-hover">'
    +'<tr>'
    +'<th>position_name</th>'
    +'<th>recruiter_name</th>'
    +'<th>company_name</th>'
    +'<th>briefing_call</th>'
    +'<th>KPIs</th>'
    +'<th>applicants</th>'
    +'</tr>'
    +'</table>'
    return htmlTable;
}

// Client Side - Registration of a Handlebars Helper.
Handlebars.registerHelper('handleListApplicants', function(listApplicants) {
  let html=''
  if(listApplicants==undefined){
    return html;
  }
  if(listApplicants.length>0){
    for(applicants in listApplicants){
      html +='<tr><td>';
      html += listApplicants[applicants].first_name + ' ' + listApplicants[applicants].first_name + '</td>';
      html += '<td>'+ listApplicants[applicants].applicant_jobs.status;
      html +='</td></tr>';
    }
  }
  return html;

});
