# job dashboard assignment

> the gatekeeper

**Please read the complete document first.**

## development

### system

We encourage you to work on a Debian-based Linux machine or VM.

### setup

1. Install docker and docker-compose
2. Install the most recent node.js lts version
3. Checkout this repo
    1. Setup `express.js` with the `src/index.js` as the starting point
    2. Start the docker containers

### environment usage

npm:

- `npm i` to install node dependencies
- `npm run migrations` to apply migrations (more info for the used package and how to use it [here](https://www.npmjs.com/package/mysql-migrations))
- `npm run seed` to seed the database, when migrations are prepared

docker:

- `docker-compose up -d` to start docker containers
- `docker-compose restart` to restart containers
- `docker-compose stop` to stop containers
- `docker-compose down` to stop and remove containers
- `docker-compose ps` to view running containers
- `docker-compose logs --follow` to follow the stdout logs of the containers

### database connection

You can find the connection details in the `docker-compose` file.

## ports

- :5306 mysql
- :9000 app

## tasks

The overall goal is to create an application based on node js and express.

There are 3 main components:

1. a mysql database that holds the data model and persists data
2. an express service that exposes an API
3. that same express service delivers the frontend to the client through templates

### data model

**jobs**

- { integer } id
- { string } position_name
- { string } recruiter_name
- { string } company_name
- { datetime } briefing_call
- { integer } contacted
- { integer } positive_response
- { integer } interview
- { integer } forward
- { datetime } closed_at
- { datetime } created_at
- { datetime } updated_at

**applicants**

- { integer } id
- { string } first_name
- { string } last_name
- { string } email
- { string } phone
- { string } current_company
- { datetime } current_position
- { datetime } created_at
- { datetime } updated_at

**applicant_jobs**

connects applicants and jobs via their IDs

- { integer } id
- { integer } applicant_id
- { integer } job_id
- { string } status
- { datetime } created_at
- { datetime } updated_at

### database

Create sql migrations to adapt the database to the data model.
Use the existing migration script to create and apply them ([package documentation](https://www.npmjs.com/package/mysql-migrations)).
To create a migration use `npm run migrations add migration <migration name>`.
The migrations are being created  in the `./migrations` directory.
When you applied the migrations to the DB you can seed it with the `seed` script.

### express api

Create a CRUD api for the entities.
We encourage you to use `sequelize.js` for connection and usage of the DB.

### frontend / dashboard

Create a simple dashboard to display a **user friendly**, filterable job list.

Following functionality should be implemented:

filterable by job id

Showing:

General Job Information

- position_name
- recruiter_name
- company_name
- briefing_call

All candidates of the job and their status

KPIs:

- positive_response/contacted
- interview/positive_response
- forward/interview


## questions

Please replace the following `TODO`s with your answer.

### What features / improvements would you add in the future?

  - input validation
  - data caching
  - client-side-rendering (actually implemented that)
  - make api calls smaller by requesting a small range of data from the apis and only update if user requests data outside the loaded range or force reloads.
  - impove the look

### How long did the assignment take you?

19 hours
  - 11 hours to get the enviroment to work propperly and including reasearch the used libraries
  - 1  hour concepts & ideas
  - 6  hours to build the app (& testing)
  - 1  hour comments and documentation

### Which part of the assignment was the most fun for you?

  - identifing problem and finding work arounds (ie. migrations didnt quit work solved it using `sequelize.js`)
  - working in a new framwork i havnt had much experience is (nodeJs & express)
  - working and understanding docker

## additional information

### submission

Please open a pull request with your changes.

### coding guidelines

- use es2015+ where applicable (const, arrow functions etc.)
- if possible, use `.editorconfig` file for basic formatting of source files

### scalability

- imagine a huge amount of data in the DB tables and implement the project accordingly to make it as easily scalable as possible in the future.

### notes on scope

- it is not necessary to specify tests to pass the assignment
- the assignment should take you **4 - 8 hours**

### bugs, problems, questions

If you think you found a bug or something is unclear, please open an issue and we will get back to you as soon as possible.
