'use strict';
module.exports = (sequelize, DataTypes) => {
  const applicants = sequelize.define('applicants', {
    id:{
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    first_name:{
      type: DataTypes.STRING
    },
    last_name:{
      type: DataTypes.STRING
    },
    email:{
      type: DataTypes.STRING
    },
    phone:{
      type: DataTypes.STRING
    },
    current_company:{
      type: DataTypes.STRING
    },
    current_position:{
      type: DataTypes.DATE
    },
    created_at:{
      type: DataTypes.DATE
    },
    updated_at:{
      type: DataTypes.DATE
    },
  },{
    timestamps: false
  });
  applicants.associate = function(models) {
     applicants.belongsToMany(models.jobs,{
       through: 'applicant_jobs',
       as : 'listJobs',
       foreignKey: 'applicant_id',
       otherKey: 'job_id'
     })
  }
  return applicants;
};
