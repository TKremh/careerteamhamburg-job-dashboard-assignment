'use strict';
module.exports = (sequelize, DataTypes) => {
  const jobs = sequelize.define('jobs', {
    id:{
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    position_name:{
      type: DataTypes.STRING
    },
    recruiter_name:{
      type: DataTypes.STRING
    },
    company_name:{
      type: DataTypes.STRING
    },
    briefing_call:{
      type: DataTypes.DATE
    },
    contacted:{
      type: DataTypes.INTEGER
    },
    positive_response:{
      type: DataTypes.INTEGER
    },
    interview:{
      type: DataTypes.INTEGER
    },
    forward:{
      type: DataTypes.INTEGER
    },
    closed_at:{
      type: DataTypes.DATE
    },
    created_at:{
      type: DataTypes.DATE
    },
    updated_at:{
      type: DataTypes.DATE
    },
  },
  {
    timestamps: false
  });
  jobs.associate = function(models) {
     jobs.belongsToMany(models.applicants,{
       through: 'applicant_jobs',
       as : 'listApplicants',
       foreignKey: 'job_id',
       otherKey: 'applicant_id'
     })
  };
  return jobs;
};
