'use strict';
module.exports = (sequelize, DataTypes) => {
  const applicant_jobs = sequelize.define('applicant_jobs', {
    id:{
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    applicant_id:{
      type: DataTypes.INTEGER,
      references:{
        model:'applicants',
        key:'id'
      }
    },
    job_id:{
      type: DataTypes.INTEGER,
      references:{
        model:'jobs',
        key:'id'
      }
    },
    status:{
      type: DataTypes.STRING
    },
    created_at:{
      type: DataTypes.DATE
    },
    updated_at:{
      type: DataTypes.DATE
    },
  },{
    timestamps: false
  });
  applicant_jobs.associate = function(models) {

  };
  return applicant_jobs;
};
